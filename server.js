const express = require('express')
const app = express()
const port = 3000

app.get('/liveness', (req, res) => res.json({ status: 'ok' }))
app.get('/oh', (req, res) => res.json({ status: 'oh yeh!!!' }))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
